require 'nokogiri'
require 'open-uri'

 namespace :scrap do

  desc "Scrap data"
  task data: :environment do
    data_with_value = {}
    urls = ["https://www.buildasign.com/SignReview.aspx?T=30667A55783975567637734934617237397852346B513D3D",
        "https://www.buildasign.com/SignReview.aspx?T=30667A5578397556763773554144416D3735714D43773D3D", "https://www.uprinting.com/vinyl-banner.html", "https://www.bannersonthecheap.com/vinyl-banner-sfp-product?docrefurl=aHR0cHM6Ly91ZHMuZG9jdW1lbnRzLmNpbXByZXNzLmlvL3YzL2RvY3VtZW50cy9iMzQyNWM5Ny00MWZhLTRhNTctYWE1Yy00YWM3MzFjNTM4YmIvZG9jcmVm&templateproductid=NDQ4OTI=&quantity=1", "https://www.buildasign.com/SignReview.aspx?T=77685A3142386F41564B6749564E4748637A6B2B46513D3D&Quantity=1", "https://www.vispronet.com/feather-flags", "https://www.smartsign.com/Safety-Sign/Custom-Attention-Sign/SKU-K-3750.aspx", "https://www.customsigns.com/yard-signs-single-sided-18-x-24"]
    urls.each do |url|
      if url.include?("www.buildasign.com")
        buildasign(url, data_with_value)
      elsif url.include?("bannersonthecheap")
        bannersonthecheap(url, data_with_value)
      elsif url.include?("www.vispronet.com")
        vispronet(url, data_with_value)
      elsif url.include?("www.smartsign.com")
        smartsign(url, data_with_value)
      elsif url.include?("www.customsigns.com")
        # customsigns(url, data_with_value) -- not working
      elsif url.include?("www.uprinting.com")
        uprinting(url, data_with_value)
      end
    end
    generate_excel(data_with_value)
  end

  def generate_excel(data_with_value)
    p = Axlsx::Package.new
    p.workbook.add_worksheet(:name => "Competition-Mapping") do |sheet|
      sheet.add_row ["URL", "Breadcrumb", "Product Name", "Product Price", "Size", "Quantity", "Product upgrade options"]
      data_with_value.each do |key, value|
        sheet.add_row value
      end
    end
    p.use_shared_strings = true
    p.serialize('Competition-Mapping.xlsx')
  end

  def buildasign(url, data_with_value)
    doc = Nokogiri::HTML(open(url))
    data_with_value[url] = []
    data_with_value[url] << url
    data_with_value[url] << ""  #breadcrump
    doc.css('#materialDropDown option').each do |node|
      data_with_value[url] << node.text.strip
    end
    doc.search('#priceText').each do |node| 
      data_with_value[url] << node.text.strip.gsub("each", "")
    end
    doc.css('#sizeDropDown option').each do |node| 
      data_with_value[url] << node.text
    end 
    doc.css('#quantityTxt').each do |m|
      data_with_value[url] << m['value'].strip
    end
    data_with_value[url] << doc.css('.upsellText').map{|option| option.text}.join("\n") #upgrade options
  end

  def bannersonthecheap(url, data_with_value)
    doc = Nokogiri::HTML(open(url))
    data_with_value[url] = []
    data_with_value[url] << url
    data_with_value[url] << ""  #breadcrump

    data_with_value[url] << "" #name
    data_with_value[url] << "" #price
    size = doc.at('#product_attribute_1081378 option[selected="selected"]').text
    data_with_value[url] << size
    quantity = doc.at('#addtocart_71313_EnteredQuantity')
    data_with_value[url] << quantity['value']
    data_with_value[url] << doc.css('#materialInput strong , em').map{|option| option.text}.join("\n") #upgrade options
  end

  def vispronet(url, data_with_value)
    doc = Nokogiri::HTML(open(url))
    data_with_value[url] = []
    data_with_value[url] << url
    data_with_value[url] << doc.at('.ty-breadcrumbs__a').text + ">" + doc.at('.ty-breadcrumbs__slash+ .ty-breadcrumbs__a').text
    data_with_value[url] << doc.at('.ty-product-block-title').text #name
    data_with_value[url] << doc.at('.ty-price-num').text + doc.at('#sec_discounted_price_683').text #price

    data_with_value[url] << "" #size
    data_with_value[url] << "" #quantity
    data_with_value[url] << "" #options
  end

  def smartsign(url, data_with_value)
    doc = Nokogiri::HTML(open(url))
    data_with_value[url] = []
    data_with_value[url] << url
    data_with_value[url] << doc.at('.yarnball').text #breadcrump
    data_with_value[url] << doc.at('.qv_head div > span').text.strip
    data_with_value[url] << doc.at('#TotalPrice').text
    data_with_value[url] << doc.at('#qvSizeLabel1').text
    data_with_value[url] << doc.at('#qvQty').values.last
    data_with_value[url] << ""
  end

  def customsigns(url, data_with_value)
    doc = Nokogiri::HTML(open(url))
    data_with_value[url] = []
    data_with_value[url] << url
    data_with_value[url] << doc.at('.yarnball').text #breadcrump
  end

  def uprinting(url, data_with_value)
    doc = Nokogiri::HTML(open(url))
    data_with_value[url] = []
    data_with_value[url] << url
    data_with_value[url] << doc.at('.breadcrumbs-not-empty').text.delete(' ').gsub("\n"," ")
    data_with_value[url] << doc.at('h1').text #name
    data_with_value[url] << '' #price
    data_with_value[url] << "" #size
    data_with_value[url] << "" #quantity
    data_with_value[url] << "" #options
  end
end
